# vimrc


## Update and Initialize Submodules

When cloning for the first time, or when you want to update the plugins, use:

```bash
git submodule update --init --recursive
```

This initializes and updates each submodule.



## Updating Submodules to Latest

To update all your submodules to the latest versions, you can run:

```bash
git submodule update --remote --merge
```

This command fetches the latest changes from the remote and merges them.
