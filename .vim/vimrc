" ~/.vim/vimrc

set nocompatible
set number
set numberwidth=1
set encoding=utf-8
set hidden
set tabstop=4
set shiftwidth=4
set expandtab
set softtabstop=4

filetype plugin indent on

" tab switching
nnoremap <Esc>a :tabprevious<CR>
nnoremap <Esc>s :tabnext<CR>

" buffer switching
nnoremap <Esc>q :bprevious<CR>
nnoremap <Esc>w :bnext<CR>

" open terminal in a new tab
nnoremap <Esc>r :tab terminal<CR>

" switch to Terminal-Normal mode
tnoremap <Esc>t <C-W>N



"
" automatically remove trailing whitespace
"
function! <SID>StripTrailingWhitespaces()
  if !&binary && &filetype != 'diff'
    let l:save = winsaveview()
    keeppatterns %s/\s\+$//e
    call winrestview(l:save)
  endif
endfun
autocmd BufWritePre,FileWritePre,FileAppendPre,FilterWritePre *
  \ :call <SID>StripTrailingWhitespaces()



"
" vim-polyglot
"
let g:polyglot_disabled = ['autoindent']
autocmd BufEnter * set indentexpr=
set conceallevel=2
let g:vim_markdown_conceal_code_blocks = 0

" Add TeX syntax highlight to .cls files as well
au BufNewFile,BufRead *.cls set filetype=tex

" Add TeX syntax highlight to .trsl (`translations` package) files as well
au BufNewFile,BufRead *.trsl set filetype=tex



"
" onedark.vim
"
packadd! onedark.vim

" Note: All options should be set before the colorscheme onedark line in your ~/.vim/vimrc
let g:onedark_terminal_italics=1

" This also needs to be done before colorscheme onedark
let g:onedark_color_overrides = {
\ "red": { "gui": "#FC5D6D", "cterm": "204", "cterm16": "1" },
\ "dark_red": { "gui": "#FC253B", "cterm": "196", "cterm16": "9" },
\ "green": { "gui": "#A9F970", "cterm": "114", "cterm16": "2" },
\ "yellow": { "gui": "#FCFC37", "cterm": "180", "cterm16": "3" },
\ "dark_yellow": { "gui": "#F9A931", "cterm": "173", "cterm16": "11" },
\ "blue": { "gui": "#A7DAE5", "cterm": "39", "cterm16": "4" },
\ "purple": { "gui": "#FA92FC", "cterm": "170", "cterm16": "5" },
\ "cyan": { "gui": "#56B6C2", "cterm": "38", "cterm16": "6" },
\ "white": { "gui": "#FFFFFF", "cterm": "145", "cterm16": "15" },
\ "black": { "gui": "#282C34", "cterm": "235", "cterm16": "0" },
\ "foreground": { "gui": "#FFFFFF", "cterm": "145", "cterm16": "NONE" },
\ "background": { "gui": "#282C34", "cterm": "235", "cterm16": "NONE" },
\ "comment_grey": { "gui": "#D8B26A", "cterm": "59", "cterm16": "7" },
\ "gutter_fg_grey": { "gui": "#4B5263", "cterm": "238", "cterm16": "8" },
\ "cursor_grey": { "gui": "#2C323C", "cterm": "236", "cterm16": "0" },
\ "visual_grey": { "gui": "#3E4452", "cterm": "237", "cterm16": "8" },
\ "menu_grey": { "gui": "#3E4452", "cterm": "237", "cterm16": "7" },
\ "special_grey": { "gui": "#3B4048", "cterm": "238", "cterm16": "7" },
\ "vertsplit": { "gui": "#3E4452", "cterm": "59", "cterm16": "7" },
\}

" Use 24-bit (true-color) mode in Vim/Neovim when outside tmux.
if (empty($TMUX))
  if (has("nvim"))
    "For Neovim 0.1.3 and 0.1.4 < https://github.com/neovim/neovim/pull/2198 >
    let $NVIM_TUI_ENABLE_TRUE_COLOR=1
  endif
  if (has("termguicolors"))
    set termguicolors
  endif
endif

" Must come after onedark configuration
syntax on
colorscheme onedark

" Set shebang syntax to Comment instead of PreProc
hi def link shShebang Comment



"
" vim-airline
"
let g:airline_theme='onedark'



"
" nerdtree
"
nnoremap <C-Bslash> :NERDTreeToggle<CR>
let g:NERDTreeLimitedSyntax = 1

" Start NERDTree when Vim is started without file arguments.
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists('s:std_in') | NERDTree | endif

" Start NERDTree when Vim starts with a directory argument.
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 1 && isdirectory(argv()[0]) && !exists('s:std_in') |
    \ execute 'NERDTree' argv()[0] | wincmd p | enew | execute 'cd '.argv()[0] |
    \ wincmd p | endif



"
" nerdcommenter
"
let g:NERDTreeShowHidden = 1
let g:NERDCreateDefaultMappings = 1
let g:NERDSpaceDelims = 1
let g:NERDDefaultAlign = 'left'
let g:NERDCommentEmptyLines = 1
let g:NERDTrimTrailingWhitespace = 1

" Toggle    CTRL + /
map <C-_> <plug>NERDCommenterToggle

" Uncomment    ESC + ,
map <Esc>, <plug>NERDCommenterUncomment

" Comment    ESC + .
map <Esc>. <plug>NERDCommenterComment

